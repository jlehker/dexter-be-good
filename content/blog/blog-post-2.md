---
image: images/blog/after-school-club.jpg
title: The After-School Book Club
author: Pam
date: 2023-11-23T15:28:43+06:00
draft: false
type: post
categories:
  - New Book
description: this is meta description
---
The After-School Book Club, a 501(c)(3) non-profit corporation
Reading Empowers Every Life

Eight years ago, two friends volunteering at an afterschool program at a Dallas elementary school decided to start a Book Club. The students had a choice. They could hang out in the cafeteria or play on the playground, or . . .  a growing number chose Book Club.


Eight years and one pandemic later, the original After-School Book Club is as strong and well-attended as ever. But the need is even greater now. The pandemic took its toll on many students, not just those in this one elementary school.


Now that The After-School Book Club is an official non-profit, the goal is to spread Clubs to other elementary schools in Dallas, particularly in disadvantaged neighborhoods. All donations are tax-deductible, and 100 percent goes to books and supplies.