---
title: "Book Reading - Jan 27"
date: 2024-01-03T15:28:43+06:00
draft: false

# post author
author : "Dexter"

# post thumb
image: "images/blog/book-reading.jpg"

# meta description
description: "Book Reading Event - January 27, 10:30 a.m."

# taxonomies
# post type
type: "post"
---

Pam McWilliams will read “Dexter Be Good!” for a special children’s story time at Interabang Books, 5600 W. Lovers Lane, Dallas, TX 75209 on January 27 2024 at 10:30 a.m.
