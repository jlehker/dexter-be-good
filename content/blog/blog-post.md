---
title: "About the Founders"
date: 2023-11-23T15:28:43+06:00
draft: false

# post author
author : "Dexter"

# post thumb
image: "images/blog/about-the-founders.png"

# meta description
description: "this is meta description"

# taxonomies
# post type
type: "post"
---
Laura Coomes, director, is a small business owner who started volunteering at a Dallas elementary school over twenty years ago. When the non-profit that ran that program folded during the pandemic, she and Pam stayed on at the school and continued the Book Club. Late in 2023, they launched The After-School Book Club as a non-profit.
Pam McWilliams, director, previously worked in Corporate Communications and on Wall Street in institutional sales. An avid reader and writer, she blogs on the arts and her fiction has appeared in several short story anthologies.
